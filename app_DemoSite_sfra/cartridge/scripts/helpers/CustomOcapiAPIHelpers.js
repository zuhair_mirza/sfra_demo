/**
* Helper script file for custom OCAPI API functions
*/

var HttpClient		   = require('dw/net/HTTPClient');
var catalogMgr 		   = require('dw/catalog/CatalogMgr');
var productMgr		   = require('dw/catalog/ProductMgr');
var productSearchModel = require('dw/catalog/ProductSearchModel');
var arrayList 		   = require('dw/util/ArrayList');

// Global Configs Variable 
var ocapiVerURL 	   = "/s/-/dw/data/v19_1/sites/RefArch/slots/"; // Global Ref Arch Version
var ocapiClientIDURL   = "/global?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; // Client ID for global
var accessTokenURL     = "/dw/oauth2/access_token?grant_type=urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

/**
 * Generate access token for Ocapi using http call
 * @param {String} base64Key - base64 key
 * @param {String} domain - current site domain for http call
 * @returns {String} access token for ocapi
 */
function generateOCAPIAccessToken(base64Key, domain){
	
	var message : String;
	var httpClient : HttpClient = new HttpClient();
	httpClient.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	httpClient.setRequestHeader("Authorization", "Basic " + base64Key);
	httpClient.setTimeout(15000);
	httpClient.open("POST", domain + accessTokenURL);
	httpClient.send();
	var reqStatus = httpClient.statusCode;
	var tempHolder : JSON;
	var accesstoken = "";
	
	if (reqStatus == '200') {
		message = httpClient.getText();		
		tempHolder = JSON.parse(message);		
		accesstoken = tempHolder["access_token"];
		
	} else {
		dw.system.Logger.error("Something wrong while generating access token using ocapi");
        throw new Error("Service is not available");
	}
	
	return accesstoken;
}

/**
 * Get content slot 1 information
 * @param {String} slotID - content slot id 
 * @param {String} accessToken - access token for http call
 * @param {String} domain - current site domain for http call
 * @returns {Object} category object collection with category id, category name and category image 
 */
function contentSlot1(slotID, accessToken, domain){
	
	var tempHolder = httpCall(slotID, accessToken, domain);
	var categoryConfig = tempHolder["slot_configurations"][0];
	var categoryData = getCategories(categoryConfig, domain);
	return categoryData;
}

/**
 * Get content slot 2 information
 * @param {String} slotID - content slot id 
 * @param {String} accessToken - access token for http call
 * @param {String} domain - current site domain for http call
 * @returns {Object} collection of products and categories
 */
function contentSlot2(slotID, accessToken, domain){
	
	var tempHolder 		= httpCall(slotID, accessToken, domain);
	var categoryConfig  = tempHolder["slot_configurations"][0];
	var productConfig   = tempHolder["slot_configurations"][1];
	var categoryArray   = getCategories(categoryConfig, domain);
	var product 		= getProduct(productConfig["slot_content"]["product_ids"], domain);
	return {
		categoryData	:	categoryArray,
		productData		:	product
	};
}

/**
 * Get content slot 3 information
 * @param {String} slotID - content slot id 
 * @param {String} accessToken - access token for http call
 * @param {String} domain - current site domain for http call
 * @returns {Object} collection of products
 */
function contentSlot3(slotID, accessToken, domain){
	
	var tempHolder = httpCall(slotID,accessToken,domain);
	var productConfig = tempHolder["slot_configurations"][0];
	return getCategoryProducts(productConfig["slot_content"]["category_ids"], 20, domain);
}

/**
 * Send http call and return response
 * @param {String} slotID - content slot id 
 * @param {String} accessToken - access token for http call
 * @param {String} domain - current site domain for http call
 * @returns {Object} JSON parse http call response and return
 */
function httpCall(slotID, accessToken, domain){
	
	var message : String;	
	var httpClient : HttpClient = new HttpClient();
	httpClient.setRequestHeader("Authorization", "Bearer " + accessToken);
	httpClient.setTimeout(15000);
	httpClient.open("GET", domain + ocapiVerURL + slotID + ocapiClientIDURL);
	httpClient.send();
	
	var reqStatus = httpClient.statusCode;
	var tempHolder : JSON;
	if (reqStatus == '200') {
		message = httpClient.getText();
		tempHolder = JSON.parse(message);
	} else {
		message="An error occurred with status code " + httpClient.statusCode;
		dw.system.Logger.error("Something wrong while calling http Call for ocapi");
        throw new Error("Ocapi Service is not available");		
	}
	return tempHolder;
}

/**
 * Get categories information
 * @param {String} config - slot configuration
 * @param {String} domain - current site domain for http call
 * @returns {Object} category object collection with category id, category name and category image 
 */
function getCategories(config, domain) {
	
	var categoriesJSON	    = {};
	categoriesJSON['Data']  = [];
	var categoryIDs 		= config["slot_content"]["category_ids"];
	
	for (var count=0; count<categoryIDs.length; count++){
		var category = catalogMgr.getCategory(categoryIDs[count]);
		var categoryImageURL = "";
		if (category.custom.slotBannerImage) {
			categoryImageURL = category.custom.slotBannerImage.getURL();
		}
		var categoryDataHolder = {
				categoryID:			category.getID(),
				categoryName:		category.getDisplayName(),
				categoryImageURL:	domain+categoryImageURL.toString()
		};
		categoriesJSON['Data'].push(categoryDataHolder);
	}
    return categoriesJSON['Data'];
}

/**
 * Get access token for Ocapi
 * @param {String} categoryID - category id 
 * @param {String} limit - counter limit 
 * @param {String} domain - current site domain for http call
 * @returns {Array} collection of products
 */
function getCategoryProducts(categoryID, limit, domain) {
	
	var productsJSON 	  = {};
	productsJSON['Data']  = [];
	var products 		  = catalogMgr.getCategory(categoryID).getProducts();
	for (var count=0; count<limit; count++){
		productsJSON['Data'].push(getProduct(products[count].getID(), domain));
	}
	return productsJSON['Data'];
}

/**
 * Get product related information
 * @param {String} productID - product id 
 * @param {String} domain - current site domain for http call
 * @returns {Object} product object with product id, name and image 
 */
function getProduct(productID, domain) {
	
	var product = productMgr.getProduct(productID);
	var productImageURl = (product.getImage('large', 0)) ? product.getImage('large', 0).getURL().toString() : "";
	var productDataHolder = {
			productID:		product.getID(),
			productName:	product.getName(),
			productImageURl: domain + productImageURl
	};
	return productDataHolder;
}

module.exports = {
		generateOCAPIAccessToken: generateOCAPIAccessToken,
	    contentSlot1: contentSlot1,
	    contentSlot2: contentSlot2,
	    contentSlot3: contentSlot3
};